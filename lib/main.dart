import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:convert';
import 'dart:async';

import 'src/views/addFabricHostForm.dart';
import 'src/views/stingrayOverviewPage.dart';
import 'src/model/fabric_host_model.dart';

void main() {
  runApp(MyApp());
}

// A function that converts a response body into a List<FabricHost>.
Future<List<FabricHost>> fetchFabricHosts() async {
  final response =
      await rootBundle.loadString("test/mock_data/mock_hosts.json");

  // Use the compute function to run parseFabricHosts in a separate isolate.
  return compute(parseFabricHosts, response);
}

// A function that converts a response body into a List<Photo>.
List<FabricHost> parseFabricHosts(String responseBody) {
  final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();

  return parsed.map<FabricHost>((json) => FabricHost.fromJson(json)).toList();
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Fabricator',
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.deepPurple,
        ),
        // home: MyHomePage(title: 'Fabricator'),
        routes: {
          '/': (context) => MyHomePage(title: 'Fabricator'),
          '/AddFabricHost': (context) => AddFabricForm(),
        });
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _popupAddFabricHost(context) {
    Navigator.pushNamed(context, '/AddFabricHost');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: FutureBuilder<List<FabricHost>>(
        future: fetchFabricHosts(),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);
          return snapshot.hasData
              ? FabricHostList(fabrichosts: snapshot.data!)
              : Center(child: CircularProgressIndicator());
        },
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          _popupAddFabricHost(context);
        },
        label: const Text('Add Fabric Host'),
        icon: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class FabricHostList extends StatelessWidget {
  final List<FabricHost> fabrichosts;

  FabricHostList({Key? key, required this.fabrichosts});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: const EdgeInsets.all(8),
      itemCount: fabrichosts.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(fabrichosts[index].hostname),
          leading: Icon(Icons.computer_rounded),
          onTap: () => (Navigator.of(context).push(MaterialPageRoute(
              builder: (context) =>
                  StingrayOverviewPage(fabrichost: fabrichosts[index])))),
        );
      },
    );
  }
}

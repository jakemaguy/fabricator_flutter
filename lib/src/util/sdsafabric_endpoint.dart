import 'dart:convert';
import 'dart:io';

import 'package:fabricator/src/model/fabric_host_model.dart';
import 'package:fabricator/src/model/stingray_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';

class SDSFabricEndpoint {
  final FabricHost fabrichost;
  // final HttpClient http;
  final http = Client();

  SDSFabricEndpoint({required this.fabrichost});

  Future<dynamic> _getEndpoint(String endpointPath) async {
    final String RELATIVE_URL =
        "http://${fabrichost.hostname}:8100/Aria/SDSfabric/1.0.0/";
    final response = await http.get(
      Uri.parse('${RELATIVE_URL}$endpointPath'),
    );
    return response.body;
  }

  // A function that converts a response body into a List<Stingray>.
  Future<List<Stingray>> parseFabricStingrays() async {
    final responseBody = await _getEndpoint('sia/info');
    final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();

    return parsed.map<Stingray>((json) => Stingray.fromJson(json)).toList();
  }

  // A function that converts a response body into a Stingray Object
  Future<Stingray> parseFabricStingray(int index) async {
    final responseBody = await _getEndpoint('sia/info/' + index.toString());
    Map<String, dynamic> parsed = jsonDecode(responseBody);
    return Stingray.fromJson(parsed);
  }

  Future<String> getFabricVersion() async {
    final responseBody = await _getEndpoint('version');
    final parsed = jsonDecode(responseBody);

    return parsed;
  }
}

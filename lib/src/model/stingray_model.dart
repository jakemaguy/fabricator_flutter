class Stingray {
  final String management_ip;
  final String management_interface;
  final String version;
  final String state;
  final String hostname;

  Stingray(
      {required this.management_ip,
      required this.management_interface,
      required this.version,
      required this.state,
      required this.hostname});

  factory Stingray.fromJson(Map<String, dynamic> parsedJson) {
    return Stingray(
      management_ip: parsedJson['management_ip'] as String,
      management_interface: parsedJson['management_interface'] as String,
      version: parsedJson['agent_info']['agent_info']['version'] as String,
      state: parsedJson['state'] as String,
      hostname: parsedJson['agent_info']['node_name'] as String,
    );
  }
}

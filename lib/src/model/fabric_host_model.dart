class FabricHost {
  final String hostname;

  FabricHost({required this.hostname});

  factory FabricHost.fromJson(Map<String, dynamic> parsedJson) {
    return FabricHost(hostname: parsedJson['hostname'] as String);
  }
}

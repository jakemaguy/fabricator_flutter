import 'dart:convert';

import 'package:fabricator/src/model/fabric_host_model.dart';
import 'package:fabricator/src/util/sdsafabric_endpoint.dart';
import 'package:fabricator/src/views/stingrayDetailPage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../model/stingray_model.dart';

class StingrayOverviewPage extends StatefulWidget {
  final FabricHost fabrichost;
  const StingrayOverviewPage({Key? key, required this.fabrichost})
      : super(key: key);

  @override
  _StingrayOverviewPageState createState() => _StingrayOverviewPageState();
}

class _StingrayOverviewPageState extends State<StingrayOverviewPage> {
  @override
  Widget build(BuildContext context) {
    final fabricendpoint = new SDSFabricEndpoint(fabrichost: widget.fabrichost);
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.fabrichost.hostname),
      ),
      body: Container(
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: FutureBuilder<List<Stingray>>(
                future: fabricendpoint.parseFabricStingrays(),
                builder: (context, snapshot) {
                  if (snapshot.hasError) print(snapshot.error);
                  return snapshot.hasData
                      ? StingrayListView(
                          stingrays: snapshot.data!,
                          fabricHost: widget.fabrichost,
                        )
                      : Center(child: CircularProgressIndicator());
                },
              ),
            ),
            Expanded(
              flex: -2,
              child: FutureBuilder<String>(
                future: fabricendpoint.getFabricVersion(),
                builder: (context, snapshot) {
                  if (snapshot.hasError) print(snapshot.error);
                  if (snapshot.hasData) {
                    return Container(
                      margin: const EdgeInsets.all(10.0),
                      child: Card(
                        child: Column(
                          children: [
                            ListTile(
                              leading: Icon(Icons.sentiment_very_satisfied),
                              title: Text("Fabric Version"),
                              subtitle: Text(snapshot.data!),
                              trailing: DropdownButton(
                                items: [],
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Expanded(
                                    flex: 2, child: Text("Latest Build: 190")),
                                TextButton(
                                  child: const Text('Upgrade Host'),
                                  onPressed: () {/* ... */},
                                ),
                                const SizedBox(width: 8),
                                TextButton(
                                  child: const Text('Fabric Logs'),
                                  onPressed: () {/* ... */},
                                ),
                                const SizedBox(width: 8),
                              ],
                            ),
                          ],
                        ),
                      ),
                    );
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class StingrayListView extends StatelessWidget {
  final List<Stingray> stingrays;
  final FabricHost fabricHost;
  // const StingrayListView(this.stingrays);
  const StingrayListView(
      {Key? key, required this.stingrays, required this.fabricHost})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: const EdgeInsets.all(8),
      itemCount: stingrays.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(stingrays[index].hostname),
          subtitle: Text(stingrays[index].state),
          trailing: Text(stingrays[index].version),
          leading: CircleAvatar(
            backgroundImage: AssetImage('images/NIC.png'),
            backgroundColor: Colors.purple[600],
          ),
          onTap: () => (Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => StingrayDetailPage(
                stingrayIndex: index,
                fabrichost: fabricHost,
              ),
            ),
          )),
        );
      },
    );
  }
}

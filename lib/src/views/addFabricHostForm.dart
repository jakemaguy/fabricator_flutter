import 'package:flutter/material.dart';

// void main() => runApp(MyApp());

class AddFabricForm extends StatefulWidget {
  const AddFabricForm({Key? key}) : super(key: key);
  // static const String routeName = '/basics/tweens';

  @override
  _AddFabricFormState createState() => _AddFabricFormState();
}

class _AddFabricFormState extends State<AddFabricForm> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add Fabric Host'),
      ),
      body: Center(
        child: Container(
          alignment: Alignment.center,
          margin: const EdgeInsets.all(20.0),
          color: Colors.blue,
          height: 200,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              TextFormField(
                decoration: const InputDecoration(
                  icon: Icon(Icons.computer_rounded),
                  hintText: 'IP Address/Hostname of Fabric Host',
                  labelText: 'Fabric Host Address',
                ),
                onSaved: (String? value) {
                  // This optional block of code can be used to run
                  // code when the user saves the form.
                },
                validator: (String? value) {
                  return (value != null && value.contains('@'))
                      ? 'Do not use the @ char.'
                      : null;
                },
              ),
              ElevatedButton(
                onPressed: () {
                  // Validate returns true if the form is valid, or false otherwise.
                  // if (_formKey.currentState!.validate()) {
                  //   // If the form is valid, display a snackbar. In the real world,
                  //   // you'd often call a server or save the information in a database.
                  //   ScaffoldMessenger.of(context).showSnackBar(
                  //       SnackBar(content: Text('Processing Data')));
                  // }
                },
                child: Text('Submit'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

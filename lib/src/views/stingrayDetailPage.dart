import 'package:fabricator/src/model/fabric_host_model.dart';
import 'package:fabricator/src/model/stingray_model.dart';
import 'package:fabricator/src/util/sdsafabric_endpoint.dart';
import 'package:flutter/material.dart';

class StingrayDetailPage extends StatefulWidget {
  final int stingrayIndex;
  final FabricHost fabrichost;
  const StingrayDetailPage(
      {Key? key, required this.stingrayIndex, required this.fabrichost})
      : super(key: key);

  @override
  _StingrayDetailPageState createState() => _StingrayDetailPageState();
}

class _StingrayDetailPageState extends State<StingrayDetailPage> {
  @override
  Widget build(BuildContext context) {
    final fabricendpoint = new SDSFabricEndpoint(fabrichost: widget.fabrichost);
    return FutureBuilder<Stingray>(
      future: fabricendpoint.parseFabricStingray(widget.stingrayIndex),
      builder: (context, snapshot) {
        if (snapshot.hasError) print(snapshot.error);
        if (snapshot.hasData) {
          final stingray = snapshot.data!;
          return Scaffold(
            appBar: AppBar(
              title: Text(stingray.hostname),
            ),
            body: Text('test'),
          );
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}
